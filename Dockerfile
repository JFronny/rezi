FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim-arm64v8 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["Rezi/Rezi.csproj", "Rezi/"]
RUN dotnet restore "Rezi/Rezi.csproj"
COPY . .
WORKDIR "/src/Rezi"
RUN dotnet build "Rezi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Rezi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Rezi.dll"]
