using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Rezi.Services;

namespace Rezi
{
    public class Program
    {
        public const string IngredientsQueryKey = "ingredients";
        public const string SearchQueryKey = "search";
        public const char QueryItemSeparator = ',';
        
        public static void Main(string[] args)
        {
            DBs.RemoveUnusedIngredients();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}