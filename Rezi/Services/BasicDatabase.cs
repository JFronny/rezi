using System.IO;
using System.Reflection;
using CC_Functions.AspNet;

namespace Rezi.Services
{
    public class BasicDatabase<T> : SerialDict<T>
    {
        public override string DatabasesDir { get; }
        public override string DatabaseFileName { get; }

        public BasicDatabase(string fileName)
        {
            DatabasesDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "DB");
            DatabaseFileName = fileName + ".db";
        }
    }
}