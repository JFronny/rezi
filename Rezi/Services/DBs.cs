using System;
using System.Collections.Generic;
using System.Linq;
using Rezi.Data;

namespace Rezi.Services
{
    public static class DBs
    {
        public static BasicDatabase<Ingredient> Ingredients = new("ingredients");
        public static BasicDatabase<Recipe> Recipes = new("recipes");

        static DBs()
        {
            Ingredients.Loaded += ingredients =>
            {
                Recipes.Mutate(recipes =>
                {
                    Dictionary<string, Guid> names = new();
                    foreach (KeyValuePair<Guid, Ingredient> kvp in ingredients)
                    {
                        string n = kvp.Value.Name;
                        if (names.ContainsKey(n))
                        {
                            ingredients.Remove(kvp);
                            foreach (Recipe recipe in recipes.Values)
                            foreach (Recipe.RecipeIngredient ingredient in recipe.Ingredients)
                                if (ingredient.Id == kvp.Key)
                                    ingredient.Id = names[n];
                        }
                        else
                            names.Add(n, kvp.Key);
                    }
                    return recipes;
                });
            };
        }

        public static void RemoveUnusedIngredients()
        {
            Recipe[] recipes = Recipes.Values.ToArray();
            Ingredients.Mutate(ingredients =>
            {
                foreach (KeyValuePair<Guid, Ingredient> ingredient in ingredients)
                    if (!recipes.Any(s => s.Ingredients.Any(s => s.Id == ingredient.Key)))
                        ingredients.Remove(ingredient);
                return ingredients;
            });
        }
    }
}
