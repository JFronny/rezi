using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Rezi.Data
{
    [ProtoContract]
    public class Recipe
    {
        [ProtoMember(1)] public string Name { get; set; }
        [ProtoMember(2)] public List<RecipeIngredient> Ingredients { get; set; }
        [ProtoMember(3)] public List<string> Steps { get; set; }

        [ProtoContract]
        public class RecipeIngredient
        {
            private string _note;
            [ProtoMember(1)] public Guid Id { get; set; }
            [ProtoMember(2)] public string Amount { get; set; }

            [ProtoMember(3)]
            public string Note
            {
                get => _note ??= "";
                set => _note = value;
            }
        }
    }
}
