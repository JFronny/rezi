using ProtoBuf;

namespace Rezi.Data
{
    [ProtoContract]
    public class Ingredient
    {
        [ProtoMember(1)] public string Name { get; set; }
    }
}